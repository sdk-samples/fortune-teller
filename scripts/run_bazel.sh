#!/bin/bash
# Copyright 2022 The Fuchsia Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

run_bazel() {
  local bazel="${REPO_ROOT}/tools/bazel"

  if [[ ! -x "${bazel}" ]]; then
    >&2 echo -n "[31;1m"
    >&2 echo "Bazel does not exist at ${bazel}. "
    >&2 echo -n "Please run the boostrap script [scripts/bootstrap.sh] and try again:"
    >&2 echo "[0m"
    exit 1
  fi

  if [[ -z "$OUTPUT_BASE" ]]; then
    "${bazel}" "$@"
  else
    "${bazel}" --output_base="${OUTPUT_BASE}" "$@"
  fi
}

