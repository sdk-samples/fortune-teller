// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "fortune.h"

#include <array>
#include <cstdlib>
#include <ctime>

std::array<std::string, 5> FORTUNES({
    "Be careful on Tuesday.",
    "Someone will call you today.",
    "You will get an A on a test.",
    "You will go to a party soon.",
    "You will have many friends.",
});

std::string random_fortune() {
  srand(time(0));
  int index = rand() % FORTUNES.size();
  return FORTUNES[index];
}

std::array<std::string, 5> all_fortunes() { return FORTUNES; }
