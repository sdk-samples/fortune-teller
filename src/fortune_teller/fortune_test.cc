// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "fortune.h"

#include <gtest/gtest.h>

#include <iostream>

TEST(FortuneTest, AddressFortune) {
  std::string fortune(random_fortune());
  bool is_valid_fortune = false;
  for (auto fortune_iter : all_fortunes()) {
    if (fortune == fortune_iter) {
      is_valid_fortune = true;
      break;
    }
  }
  EXPECT_TRUE(is_valid_fortune);
}
