// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef SRC_FORTUNE_TELLER_FORTUNE_H_
#define SRC_FORTUNE_TELLER_FORTUNE_H_

#include <array>
#include <string>

std::string random_fortune();

std::array<std::string, 5> all_fortunes();

#endif  // SRC_FORTUNE_TELLER_FORTUNE_H_
