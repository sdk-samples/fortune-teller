// Copyright 2022 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <lib/syslog/global.h>

#include "fortune.h"

int main() {
  std::string fortune(random_fortune());
  FX_LOG(INFO, "fortune-teller", fortune.c_str());
  return 0;
}
