# SDK Samples: Fortune Teller

This repository contains instructions and source code to build, package and run
Fuchsia samples using only the Fuchsia SDK.

## Requirements

1. Make sure this repository has the required submodules:

   ```
   git submodule update --recursive --init
   ```
   
1. Run bootstrap to download bazel

   ```
   scripts/bootstrap.sh
   ```

1. Ensure that there are Fuchsia SSH keys in your host machine. You will need
   them for running the Fuchsia emulator.

   ```
    [[ -f "${HOME}/.ssh/fuchsia_ed25519" ]] || ssh-keygen -P "" -t ed25519 -f "${HOME}/.ssh/fuchsia_ed25519" -C "${USER}@$(hostname -f) Shared SSH Key for Fuchsia"

    [[ -f "${HOME}/.ssh/fuchsia_authorized_keys" ]] || ssh-keygen -y -f "${HOME}/.ssh/fuchsia_ed25519" > "${HOME}/.ssh/fuchsia_authorized_keys"
   ```

## Build and package the sample

Now the repository is ready to build the sample.

1. Prepare the development environment

   ```
   tools/bazel run :ws_emu_development
   ```

1. (optional) Verify that your enviroment is correctly set up

   ```
   tools/bazel run :ws_emu_development.status
   ```

1.. (optional) watch the device log in a separate window

   ```
   tools/ffx log
   ```

1.. Run the fortune-teller component

   ```
   bazel run //src/fortune_teller:pkg.component
   ```

   Watch that your fortune is printed in the log.


## Run Tests

1. Start emulator (see step #1 above)

2. Run tests

   ```
   bazel test --config=fuchsia_x64 //src/fortune_teller:test_pkg
   ```

